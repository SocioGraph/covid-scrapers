#! /usr/bin/python
from flask import Flask, request, redirect, url_for,render_template,flash,jsonify,session,Markup,abort,send_file,after_this_request,make_response
import os
from werkzeug.exceptions import BadRequest
import requests
import base64
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, sep as dirsep,isfile,splitext
import helpers as hp
import time
import importlib.util
import pkgutil
import json

HTTP = os.environ.get('HTTP', 'http')
SERVER_NAME = os.environ.get('SERVER_NAME', 'localhost:5004')
BASE_DIR_PATH = dirname(abspath(__file__))
app = Flask(__name__)
app.config['LOGIN_URL'] = 'login'
app.config['ERROR_URL'] = 'error'
app.config['MAX_CONTENT_LENGTH'] = 5 * 1024 * 1024 * 1024
app.config['PREFERRED_URL_SCHEME'] = HTTP
app.config['TEMPLATES_AUTO_RELOAD'] = True if 'localhost' in SERVER_NAME else False
app.config['UPLOAD_FOLDER'] = 'static/uploads'
app.secret_key = 'rudido really like this but it is not long enough932184093848naskdj((&*(&*(&&*%^&$%%$##$@#$@!@#!#!'
debug = os.environ.get('DEBUG', "True").lower() == "true"
app.testing = debug
port = 5004
host = '0.0.0.0'
scraper_modules = dict(map((lambda x: (x[1], x[0].find_module(x[1]).load_module(x[1]))), pkgutil.iter_modules(['scrapers'])))
logger = hp.get_logger(__file__)

@app.route("/<name>")
def run_scraper(name):
    ts = time.time()
    if not scraper_modules.get(name):
        return jsonify(error = "Error importing {} - No such file".format(name)), 500, {"Access-Control-Allow-Origin": "*"}
    try:
        logger.info("Before calling the run")
        r = scraper_modules[name].run()
        logger.info("Got the results of the run")
    except Exception as e:
        hp.print_error(e)
        return jsonify(error = "Error executing {} - {}".format(name,e)), 500, {"Access-Control-Allow-Origin": "*"}
    return jsonify(info = "Ran scraper {} in {:.2f} sec: Result = {}".format(name, time.time() - ts, str(r)))


@app.route("/")
def run_scrapers():
    ts = time.time()
    rl = []
    for k, v in scraper_modules.items():
        try:
            logger.info("Running scrapper: %s", k)
            r = v.run()
            rl.append(r)
        except Exception as e:
            hp.print_error(e)
    return jsonify(info = "Ran all scrapers in {:.2f} sec: Result = {}".format(time.time() - ts, json.dumps(rl, indent = 4)))

if __name__=="__main__":
    app.run(host=host, port = port, debug = debug)


***This is a project to support [covidindiahelp.info](https://www.covidindiahelp.info) collate more sources of people full-filling the need of Indians in crisis.***

Please refer to this project if you want to contribute to the following aspects in the covidindiahelp.info project: 
- React JS developers
- Node JS development 
Please checkout this open source project: https://github.com/akashtakyar/covidindiahelp-web/issues/1

**If you want to help, follow these steps**

Use Python3.8 environment
```
pip install -r requirements.txt
```

1. Create a unique python file (adding the website you are crawling in the name will help) in the directory "scrapers"

2. Write your own way to parse the website (Selenium, Beautiful Soup, etc.)

3. If you need any libraries to run your code, have a look at requirements.txt, add appropriately. Be careful not to add unnecessary libraries

4. Add these lines to your code (preferrably in the beginning)

```
import sys
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, sep as dirsep,isfile,splitext
sys.path.insert(0, joinpath(dirname(dirname(abspath(__file__)))))
import helpers as hp
```                                                                                                                                            

5. Each result you are getting from your website should be formatted to this dict:
```
{
	"description":   <some description of the availablity> (required),
	"category": <category >(required, options: ["Bed", "Blood Plasma", "Oxygen", "Remdesivir", "Fabiflu", "Tocilizumab"]),
	"state": <state >(required),
	"district": <district/city/area> (not obligatory, but prefered),
	"phoneNumber": [<ph1>, <ph2>]   (list of phone numbers, at least one is required),
	"addedOn": <linux timestamp (time.time())> (required, if it is not an update),
	"modifiedOn": <linux timestamp (time.time())>  (required if created On is not provided)
}
```
e.g.
```
params = {
	"desription": "Bed available in Safdarjung hospital"
	"category": "Bed",
	"state": "Delhi",
	"district": "Delhi",
	"phoneNumber": ["9980838165"],
	"addedOn": 1619872940.323493,
	"modifiedOn": 1619872940.323493
}
```

6. For each such result call the function
```
response = hp.send(params)
```

7. If there is any error in sending this data, the response will be
```
{"error": <Reason for failure is ... >
```

8. If successful, the response will be like this
```
{   
	"_id": "608bbdb0e750af0034db6f26",
	"message": <Message about the data sent>
}
```

9. You may save this 'id' and any other information you want to save in this manner. For example many parsers can store the timestamp (string) for when they last ran. The next time you can utilize it to only send data updated after the last run.
```
hp.save(<id>, data)
```
To retrieve:
```
hp.get(<id>)
``` 
Try not to save a lot of data because we are trying to save resources. However, you can use this data to check if your data was previously sent to the DB and not send obsolete information. Anything that can be written to json can be used. If you want to run it locally, it is just a file structure.
10. At the end of the file please add this code
```
if __name__ == "__main__":
    run()
```
Make sure the run command can go through your URL and update all the latest and relevant data one time only. Don't try and add any scheduling or scheduler methods or functions.
11. __NOTE__ - Please make sure you can run your scraper once by executing using the command 
```python scrapers/<name of your file.py>```
12. Please commit and create a pull request

Please contact ananth@iamdave.ai if you have any questions, queries or problems

Join Slack channel: https://bit.ly/3t6NBfd



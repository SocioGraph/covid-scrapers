#!/bin/bash
d=$( dirname $( readlink -f $0 ) )
for l in `ls $d/scrapers/*.py`; do
    $d/.env/bin/python $l 2>&1 | tee -a $d/log.log
done
killall chrome
killall chromium-browser
pkill chrome
pkill chromium-browser

import sys
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, sep as dirsep,isfile,splitext
sys.path.insert(0, joinpath(dirname(dirname(abspath(__file__)))))
import requests
import pandas as pd
import helpers as hp
import phonenumbers

def convert_phone(string):
    lo = []
    str1 = string.replace("/", ",")
    str2 = str1.replace("-", "")
    str3 = str2.replace(" ", "")
    li = list(str3.split(','))
    for l in li:
        try:
            ph_no = phonenumbers.parse(l, "IN").national_number
            lo.append(str(ph_no))
        except:
            pass
    return lo

def scrape(last_data=None):
    # Andhrapradhesh
    data_ap = requests.get('https://api.covidbedsindia.in/v1/storages/608982e003eef31f34d05a71/Andhra Pradesh').json()
    df_ap1 = pd.DataFrame.from_dict(data_ap)
    df_ap1['description'] = ', ICU_beds_available:' + df_ap1['ICU_AVAILABLE'] \
                            + ", Oxygen_beds_general_avail:" + df_ap1['OXYGEN_GENERAL_AVAILABLE'] \
                            + ", General_beds_available:" + df_ap1['GENERAL_AVAILABLE'] \
                            + ", Ventilator:" + df_ap1['VENTILATOR']
    df_ap = df_ap1[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']]

    # Pune
    data_pune = requests.get('https://api.covidbedsindia.in/v1/storages/6089822703eef30c1cd05a6e/Pune').json()
    data_pune1 = pd.DataFrame.from_dict(data_pune)
    data_pune1['STEIN_ID'] = 'Maharashtra'
    data_pune1['description'] = ', Oxygen_beds_general_avail:' + data_pune1['AVAILABLE_BEDS_WITH_OXYGEN'] \
                                + ' , No_oxygen_beds_available:' + data_pune1['AVAILABLE_BEDS_WITHOUT_OXYGEN'] \
                                + ", Ventilator:" + data_pune1['AVAILABLE_ICU_BEDS_WITH_VENTILATOR'] \
                                + " ,Beds_without_ventilator:" + data_pune1['AVAILABLE_ICU_BEDS_WITHOUT_VENTILATOR']
    df_pune = data_pune1[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']]

    # Ghandinagar
    data_ghan = requests.get('https://api.covidbedsindia.in/v1/storages/608d544533382c3501cf8c96/Gandhinagar').json()
    data_ghan1 = pd.DataFrame.from_dict(data_ghan)
    data_ghan1['STEIN_ID'] = 'Gujarat'
    data_ghan1['DISTRICT'] = data_ghan1['AREA'] + ", Ghandinagar"
    data_ghan1['CONTACT'] = data_ghan1['NODAL_OFFICER_NUMBER']
    data_ghan1['description'] = ',Mild_symptoms_beds_available:' + data_ghan1['MILD_SYMPTOMS_BED_AVAILABLE'] \
                                + ", Oxygen_beds_avail:" + data_ghan1['O2_BEDS_AVAILABLE'] \
                                + ", ICU_without_Ventilator:" + data_ghan1['ICU_WITHOUT_VENTILATOR_AVAILABLE'] \
                                + ", ICU_Ventilator:" + data_ghan1['ICU_WITH_VENTILATOR_AVAILABLE']
    df_ghan = data_ghan1[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']]

    # extracting for West Bengal
    data_wb = requests.get('https://api.covidbedsindia.in/v1/storages/608983fc03eef384cad05a78/West%20Bengal').json()
    df_data_wb = pd.DataFrame.from_dict(data_wb)
    df_data_wb['description'] = ', covid_beds_regular_available: ' + df_data_wb['COVID_BEDS_REGULAR_VACANT'] \
                                + ", covid_beds_with_oxygen_available: " + df_data_wb['COVID_BEDS_WITH_OXYGEN_VACANT'] \
                                + ", hdu_beds_vacant :" + df_data_wb['HDU_BEDS_VACANT'] \
                                + ", ccu_beds_without_ventilator_available:" + df_data_wb[
                                    'CCU_BEDS_WITHOUT_VENTILATOR_VACANT'] \
                                + ", ccu_beds_with_ventilator_available:" + df_data_wb[
                                    'CCU_BEDS_WITH_VENTILATOR_VACANT']
    df_wb = df_data_wb[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']]

    # extracting for uttarkhand
    data_ut_kh = requests.get('https://api.covidbedsindia.in/v1/storages/608d542333382c01aecf8c95/Uttarakhand').json()
    df_data_uttar = pd.DataFrame.from_dict(data_ut_kh)
    df_data_uttar['description'] = ', beds_without_oxygen_available: ' + df_data_uttar['BEDS_WITHOUT_OXYGEN_AVAILABLE'] \
                                   + ', beds_with_oxygen_available:' + df_data_uttar['BEDS_WITH_OXYGEN_AVAILABLE'] \
                                   + ', icu_beds_available' + df_data_uttar['ICU_BEDS_AVAILABLE']
    df_data_uttar['CONTACT'] = df_data_uttar['NODAL_OFFICER_NUMBER']
    df_utkh = df_data_uttar[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']]

    # extracting for ahamedabad
    data_ahmed = requests.get('https://api.covidbedsindia.in/v1/storages/608982c803eef3543bd05a70/Ahmedabad').json()
    df_data_ahmed = pd.DataFrame.from_dict(data_ahmed)
    df_data_ahmed['description'] = ', amc_isolation_available:' + df_data_ahmed['AMC_ISOLATION_VACANT'] \
                                   + ', amc_high_dependency_unit_available:' + df_data_ahmed[
                                       'AMC_HIGH_DEPENDENCY_UNIT_VACANT'] \
                                   + ', amc_icu_without_ventilator_available:' + df_data_ahmed[
                                       'AMC_ICU_WITHOUT_VENTILATOR_VACANT'] \
                                   + ', amc_icu_with_ventilator_available:' + df_data_ahmed[
                                       'AMC_ICU_WITH_VENTILATOR_VACANT'] \
                                   + ', private_isolation_available:' + df_data_ahmed['PRIVATE_ISOLATION_VACANT'] \
                                   + ', private_high_dependency_available:' + df_data_ahmed[
                                       'PRIVATE_HIGH_DEPENDENCY_UNIT_VACANT'] \
                                   + ', private_icu_without_ventilator_available:' + df_data_ahmed[
                                       'PRIVATE_ICU_WITHOUT_VENTILATOR_VACANT'] \
                                   + ', private_icu_with_ventilator_availble:' + df_data_ahmed[
                                       'PRIVATE_ICU_WITH_VENTILATOR_VACANT']
    df_data_ahmed['STEIN_ID'] = 'GUJARAT'
    df_data_ahmed['DISTRICT'] = df_data_ahmed['ZONE_WARDS'] + ", Ahmedabad"
    df_ahme = df_data_ahmed[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']].applymap(str)
    df_ahmed = df_ahme.loc[df_ahme['CONTACT'] != 'None'].applymap(str)

    # extracting for UttarPradesh
    data_up = requests.get('https://api.covidbedsindia.in/v1/storages/609fc7dde75f9ccdd696eb35/Uttar%20Pradesh').json()
    df_data_up = pd.DataFrame.from_dict(data_up)
    df_data_up['description'] = ', available_beds:' + df_data_up['VACANT']
    df_uph = df_data_up[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']].applymap(str)
    df_uph['CONTACT'] = df_uph['CONTACT'].str.strip()
    df_uph['CONTACT'] = df_uph['CONTACT'].replace('', 'empty')
    df_up = df_uph.loc[df_uph['CONTACT'] != 'empty'].applymap(str)

    # extracting for Thane
    data_TH = requests.get('https://api.covidbedsindia.in/v1/storages/609fc78ae75f9c111f96eb34/Thane').json()
    df_covid_data_TH = pd.DataFrame.from_dict(data_TH)
    df_covid_data_TH['description'] = ', ICU_beds_available: ' + df_covid_data_TH['TOTAL_ICU_BEDS_AVAILABLE'] \
                                      + ", General_beds_available:" + df_covid_data_TH['TOTAL_BEDS_AVAILABLE']
    df_covid_data_TH['DISTRICT'] = 'Thane'
    df_covid_data_TH['STEIN_ID'] = 'Maharashtra'
    df_covid_data_TH['CONTACT'] = df_covid_data_TH['HOSPITAL_NUMBER']
    df_than = df_covid_data_TH[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']].applymap(str)
    df_thane = df_than.loc[df_than['CONTACT'] != 'None'].applymap(str)

    # extracting for Surat
    data_SU = requests.get('https://api.covidbedsindia.in/v1/storages/6094e879423e213eb82fd384/Surat').json()
    df_covid_data_SU = pd.DataFrame.from_dict(data_SU)
    df_covid_data_SU['description'] = ', Oxygen_beds_general_avail:' + df_covid_data_SU['TOTAL_OXYGEN_BEDS_AVAILABLE'] \
                                      + ", General_beds_available:" + df_covid_data_SU['TOTAL_WARD_BEDS_AVAILABLE'] \
                                      + ", Ventilator:" + df_covid_data_SU['TOTAL_VENTILATOR_BEDS_AVAILABLE']
    df_covid_data_SU['DISTRICT'] = 'Surat'
    df_covid_data_SU['STEIN_ID'] = 'Gujarat'
    df_covid_data_SU['CONTACT'] = df_covid_data_SU['HOSPITAL_NUMBER']
    df_sura = df_covid_data_SU[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']].applymap(str)
    df_surat = df_sura.loc[df_sura['CONTACT'] != 'None'].applymap(str)

    # extracting for Tamilnadu
    data_TN = requests.get('https://api.covidbedsindia.in/v1/storages/6089820e03eef3b588d05a6d/Tamil%20Nadu').json()
    df_covid_data_TN = pd.DataFrame.from_dict(data_TN)
    df_covid_data_TN['description'] = ', ICU_beds_available: ' + df_covid_data_TN['ICU_BEDS_VACANT'] \
                                      + ", Oxygen_beds_general_avail:" + df_covid_data_TN[
                                          'OXYGEN_SUPPORTED_BEDS_VACANT'] \
                                      + ", General_beds_available:" + df_covid_data_TN[
                                          'BEDS_FOR_SUSPECTED_CASES_VACANT'] \
                                      + ", Ventilator:" + df_covid_data_TN['VENTILATOR_VACANT']
    df_tnd = df_covid_data_TN[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']]
    df_tn = df_tnd.loc[df_tnd['CONTACT'] != 'NULL'].applymap(str)

    # extracting for Telangana
    data_TG = requests.get('https://api.covidbedsindia.in/v1/storages/6089829403eef36d93d05a6f/Telangana').json()
    df_covid_data_TG = pd.DataFrame.from_dict(data_TG)
    df_covid_data_TG['description'] = ', ICU_beds_available: ' + df_covid_data_TG['ICU_BEDS_VACANT'] \
                                      + ", Oxygen_beds_general_avail:" + df_covid_data_TG['OXYGEN_BEDS_VACANT'] \
                                      + ", General_beds_available:" + df_covid_data_TG['REGULAR_BEDS_VACANT']
    df_tel = df_covid_data_TG[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']]

    # Extract for Madhya Pradesh
    data_MP = requests.get('https://api.covidbedsindia.in/v1/storages/608983ca03eef3dd7ad05a75/Madhya%20Pradesh').json()
    df_covid_data_MP = pd.DataFrame.from_dict(data_MP)
    df_covid_data_MP['description'] = ', ICU_beds_available: ' + df_covid_data_MP['ICU_BEDS_AVAILABLE'] \
                                      + ", Oxygen_beds_general_avail:" + df_covid_data_MP['OXYGEN_BEDS_AVAILABLE'] \
                                      + ", General_beds_available:" + df_covid_data_MP['ISOLATION_BEDS_AVAILABLE']
    df_covid_data_MP['STEIN_ID'] = 'Madhya Pradesh'
    df_covid_data_MP['CONTACT'] = df_covid_data_MP['CONTACT_1'] + "," \
                                  + df_covid_data_MP['CONTACT_2'] + "," \
                                  + df_covid_data_MP['CONTACT_3']
    df_mpsh = df_covid_data_MP[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']].applymap(str)
    df_mp = df_mpsh.loc[df_mpsh['CONTACT'] != 'nan'].applymap(str)

    # Extract for Rajasthan
    data_RJ = requests.get('https://api.covidbedsindia.in/v1/storages/608983ed03eef39bb4d05a77/Rajasthan').json()
    df_covid_data_RJ = pd.DataFrame.from_dict(data_RJ)
    df_covid_data_RJ['description'] = ', ICU_beds_available: ' + df_covid_data_RJ['ICU_BEDS_AVAILABLE'] \
                                      + ", Oxygen_beds_general_avail:" + df_covid_data_RJ['OXYGEN_BEDS_AVAILABLE'] \
                                      + ", General_beds_available:" + df_covid_data_RJ['GENERAL_BEDS_AVAILABLE'] \
                                      + ", Ventilator_beds_available:" + df_covid_data_RJ['VENTILATOR_BEDS_AVAILABLE']
    df_covid_data_RJ['CONTACT'] = df_covid_data_RJ['HOSPITAL_HELPLINE_NO']
    df_rjs = df_covid_data_RJ[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']].applymap(str)
    df_rj = df_rjs.loc[df_rjs['CONTACT'] != 'NA'].applymap(str)

    # Extract for Nashik
    data_NA = requests.get('https://api.covidbedsindia.in/v1/storages/608efad8423e2153ac2fd383/Nashik').json()
    df_covid_data_NA = pd.DataFrame.from_dict(data_NA)
    df_covid_data_NA['description'] = ',' + df_covid_data_NA['HOSPITAL_ADDRESS'] \
                                      + ', ICU_beds_available: ' + df_covid_data_NA['ICU_BEDS_AVAILABLE'] \
                                      + ", Oxygen_beds_general_avail:" + df_covid_data_NA['BEDS_WITH_OXYGEN_AVAILABLE'] \
                                      + ", General_beds_available:" + df_covid_data_NA['NMC_RESERVED_BED_AVAILABLE'] \
                                      + ", Ventilator_beds_available:" + df_covid_data_NA['VENTILATOR_AVAILABLE']
    df_covid_data_NA['CONTACT'] = df_covid_data_NA['HOSPITAL_NUMBER'] + "," + df_covid_data_NA[
        'HOSPITAL_LANDLINE_NUMBER']
    df_covid_data_NA['STEIN_ID'] = 'Maharashtra'
    df_covid_data_NA['DISTRICT'] = 'Nashik'
    df_nash1 = df_covid_data_NA[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']].applymap(str)
    df_nash = df_nash1.loc[df_nash1['CONTACT'] != 'nan'].applymap(str)

    # Extract for Nagpur
    data_Nag = requests.get('https://api.covidbedsindia.in/v1/storages/608983e003eef32328d05a76/Nagpur').json()
    df_covid_data_Nag = pd.DataFrame.from_dict(data_Nag)
    df_covid_data_Nag['description'] = ',' + df_covid_data_Nag['ADDRESS'] + \
                                       ', ICU_beds_available: ' + df_covid_data_Nag['TOTAL_ICU_BEDS_AVAILABLE'] \
                                       + ", Oxygen_beds_general_avail:" + df_covid_data_Nag[
                                           'TOTAL_OXYGEN_BEDS_AVAILABLE'] \
                                       + ", General_beds_available:" + df_covid_data_Nag[
                                           'TOTAL_WITHOUT_OXYGEN_BEDS_AVAILABLE'] \
                                       + ", Ventilator_beds_available:" + df_covid_data_Nag[
                                           'TOTAL_VENTILATOR_BEDS_AVAILABLE']
    df_covid_data_Nag['STEIN_ID'] = 'Maharashtra'
    df_covid_data_Nag['DISTRICT'] = 'Nagpur'
    df_covid_data_Nag['CONTACT'] = df_covid_data_Nag['HOSPITAL_NUMBER']
    df_nag = df_covid_data_Nag[['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']]

    #appending all df and converting to object
    df_covid_data_1 = pd.DataFrame().append(
        [df_ap, df_pune, df_ghan, df_wb, df_utkh, df_ahmed, df_up, df_thane, df_surat, df_tn, df_tel, df_mp, df_rj,
         df_nash,
         df_nag])
    df_covid_data2 = df_covid_data_1.applymap(str)

    #removing null phone values
    df_covid_data2['CONTACT'] = df_covid_data2['CONTACT'].str.strip()
    df_covid_data2['CONTACT'] = df_covid_data2['CONTACT'].replace('', 'empty')
    df_covid_data = df_covid_data2.loc[df_covid_data2['CONTACT'] != 'empty'].applymap(str)

    print(len(df_covid_data.index))
    df_final_loaded = df_covid_data

    if last_data:
        df_last_run = pd.read_json(last_data)
        df_last_run_data = df_last_run.applymap(str)
        df_last_run_data.rename(columns=lambda x: x + '_df_last_run_data', inplace=True)
        df_covid_data.rename(columns=lambda x: x + 'df_covid_data', inplace=True)

        df_join = df_last_run_data.merge(right=df_covid_data,
                                         left_on=df_last_run_data.columns.to_list(),
                                         right_on=df_covid_data.columns.to_list(),
                                         how='outer')

        present_in_df_scraped_data_not_in_df_last_run_data = df_join.loc[df_join[df_last_run_data.columns.to_list()]
                                                                             .isnull().all(
            axis=1), df_covid_data.columns.to_list()]

        df_covid_data = present_in_df_scraped_data_not_in_df_last_run_data
        df_covid_data.columns = ['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']
        df_last_run_data.columns = ['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT', 'description']
        print(f'df_last_run_data count:{len(df_last_run_data.index)}')
        df_covid_data.to_json('sent_data.json')
        df_last_run_data.to_json('last_run_data.json')
        df_final_loaded = df_last_run_data.append(df_covid_data)

        df_final_loaded.drop_duplicates(['STEIN_ID', 'DISTRICT', 'HOSPITAL_NAME', 'CONTACT'], keep='last', inplace=True)

    error_count = 0
    sent_count = 0
    for index, row in df_covid_data.iterrows():
        try:
            hp.send({
                "description": row['HOSPITAL_NAME'] + " " + row['description'],
                "category": 'Beds',
                "state": row['STEIN_ID'],
                "district": row['DISTRICT'],
                "phoneNumber": convert_phone(row['CONTACT'])
            })
            sent_count = sent_count + 1
        except:
            error_count = error_count + 1
            print("Error_Data")
            print(row['HOSPITAL_NAME'])
            print(convert_phone(row['CONTACT']))

    print(f'Count of Records loaded is {sent_count} and not loaded: {error_count}')

    return_result = df_final_loaded.to_json(orient="records")
    return return_result

def run():
    hp.ENVIRONMENT = "dev"
    last_data = hp.get('prev_data')
    results = scrape(last_data)
    hp.save('prev_data', results)
    return results

if __name__ == "__main__":
    run()

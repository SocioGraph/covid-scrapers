import pandas as pd
import sys
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, sep as dirsep,isfile,splitext
sys.path.insert(0, joinpath(dirname(dirname(abspath(__file__)))))
import helpers as hp
from datetime import datetime
import datetime
logger = hp.get_logger(__name__)

"""
@author: Mahitha
This script scrapes the Bihar covid website and checks for beds availability and publish to end point.
Used pandas util to parse the content.
Script ONLY pushes the records which are updated in the last 3 hours.
"""

def load_url():
    global df
    url = 'https://covid19health.bihar.gov.in/DailyDashboard/BedsOccupied'
    tables = pd.read_html(url)
    df = tables[0]

load_url()

def parse_table():
    global data
    logger.info("Parsing the table on this site")
    data = pd.DataFrame(columns=['description', 'category', 'state', 'district', 'phoneNumber', 'modifiedOn'])
    data.district = df.District
    data.phoneNumber = ((df.Contact).astype(str)).tolist()
    data['category'] = 'Beds'
    data['state'] = 'Bihar'
    data.modifiedOn = df['Last Updated']
    data['description'] = 'At ' + df['Facility Name'] + ' ' + df['Vacant'].astype(str) + ' beds are available , ' + df[
        'ICU Beds Vacant'].astype(str) + ' icu beds are available. '
    data['modifiedOn'] = pd.to_datetime(data['modifiedOn']).dt.tz_localize('Asia/Kolkata')


parse_table()

def run():
    last_run = hp.to_datetime(hp.get('last_run')) or (hp.now() - datetime.timedelta(hours=3))
    result_df = (data[data['modifiedOn'] >= last_run])
    print(result_df)
    # result_df will have records updated only in last 3 hours
    if len(result_df) != 0:
        for row_dict in result_df.to_dict(orient="records"):
            try:
                hp.send(row_dict)
                pass
            except Exception as e:
                hp.print_error(e)
    else:
        print("skipping records push as no updated data found")
    hp.save('last_run', str(hp.now()))


if __name__ == "__main__":
    run()






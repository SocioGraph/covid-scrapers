import sys
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, sep as dirsep,isfile,splitext
sys.path.insert(0, joinpath(dirname(dirname(abspath(__file__)))))
import helpers as hp
import requests
from bs4 import BeautifulSoup
import json 
logger = hp.get_logger(__name__)

def load_url(*args, **kwargs):
    URL = 'http://164.100.112.24/SpringMVC/getHospital_Beds_Status_Citizen.htm'

    post_params = [{'hospital': 'G'},{'hospital': 'P'}]
    s = requests.session()
    result = []

    for i in range(len(post_params)):
        response = s.post(URL, data=post_params[i])
        soup_obj = BeautifulSoup(response.text, 'html.parser')
        res = scrape(soup_obj, *args, **kwargs)
        result.extend(res)

    return result
# end of function 

def scrape(soup, last_run = None, stale_hours = 3):
    # function to scrape the website contents
    if last_run:
        last_run = max(last_run, hp.now() - hp.timedelta(hours = stale_hours))
    else:
        last_run =  hp.now() - hp.timedelta(hours = stale_hours)
    data = []
    cols = ['description', 'category','state', 'district', 'phoneNumber', 'modifiedon']

    table = soup.find_all('table')[0]
    rows = table.find_all('tr')[3:-1]

    districts = table.find_all('td', 
                attrs={'style': 'vertical-align: middle;text-align: center;background-color: none;'})[1::2]
    hospitals = table.find_all('td', attrs={'style': 'text-align: left;background-color: none;'})

    for district in districts:
        span = int(district.attrs['rowspan'])
        r_hospitals = hospitals[:span]
        r_rows = rows[:span]
        curr_info = {}
        
        for i in range(len(r_hospitals)):
            lst = []
            h = r_hospitals[i]
            r = r_rows[i]

            values = [i.text.strip() for i in r.find_all('td')[-3:]]
            ph = [j.text.strip() for j in r.find_all('td', 
                            attrs={'style': 'text-align:  center;background-color: none; max-width: 100px;'})]

            if(int(values[0]) > 0 and len(ph) != 0):
                ph1 = list(map(lambda x: x.strip(), ph[0].split(',')))
                desc = 'Beds available in ' + h.text[2:] + ' hospital'
                cat = "Beds"
                st = 'Telangana'
                dt = district.text
                mod = hp.to_datetime(values[1] + ' ' + values[2], dayfirst = True)
                # Skip if timestamp before last updated
                if mod > last_run:
                    lst.extend([desc,cat,st,dt,ph1,mod])
                    #to convert lists to dictionary
                    curr_info = dict(zip(cols, lst))
                    try:
                        hp.send(curr_info)
                    except Exception as e:
                        hp.print_error(e)
                    curr_info['modifiedOn'] = str(curr_info['modifiedOn'])
                    data.append(curr_info)
                else:
                    logger.info("Skipping record modified on %s before %s", mod, last_run)
            else:
                continue
        hospitals = hospitals[span:]
        rows = rows[span:]

    return data        
#end of function

def run():
    last_run = hp.to_datetime(hp.get('last_run'))
    logger.info("Before parse table, last run: %s", last_run)
    results = load_url(last_run)
    hp.save('last_run', str(hp.now()))
    return json.dumps(results, indent = 4)
    
# end of function

if __name__ == "__main__":
    run()

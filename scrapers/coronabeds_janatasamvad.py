import sys
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, sep as dirsep,isfile,splitext
sys.path.insert(0, joinpath(dirname(dirname(abspath(__file__)))))
import helpers as hp
from datetime import datetime
from requests_html import AsyncHTMLSession
import json
import asyncio
import pyppeteer
import tracemalloc
logger = hp.get_logger(__name__)

URLs = (
        ("https://coronabeds.jantasamvad.org/beds.html", "Beds"),
        ("https://coronabeds.jantasamvad.org/all-covid-icu-beds.html", "ICU Beds")
)


def parse_table(soup, last_run, identifier = "hospitals_list", description = "Beds"):
    logger.info("Parsing for %s", description)
    tbl = soup.find("#{}".format(identifier), first = True)
    take_phone = {}
    for tr in tbl.lxml.getchildren()[0]:
        ch = tr.getchildren()
        if 'collapse' in tr.attrib.get('id', ''):
            if take_phone:
                take_phone['phoneNumber'] = ch[0].getchildren()[0].getchildren()[1].getchildren()[1].getchildren()[0].text_content().strip()
                take_phone['state'] = 'Delhi'
                take_phone['district'] = 'Delhi'
                take_phone['category'] = "Bed"
                yield take_phone
            else:
                continue
        try:
            hosp = ch[0].getchildren()[1].text_content().strip()
            rtime = hp.to_datetime(ch[1].text_content().strip())
            beds = ch[3].getchildren()[0].text_content().strip()
            if rtime > last_run and int(beds) > 0:
                take_phone = {"description": "{} {} available in {}".format(beds, description, hosp), "addedOn": rtime.timestamp()}
            else:
                take_phone = {}
        except IndexError:
            continue

async def load_url(session, url = None, last_run = None, stale_hours = 3, **kwargs):
    if last_run:
        last_run = max(last_run, hp.now() - hp.timedelta(hours = stale_hours))
    else:
        last_run =  hp.now() - hp.timedelta(hours = stale_hours)
    soup = await session.get(url)
    await soup.html.arender()
    results = []
    for res in parse_table(soup.html, last_run, **kwargs):
        hp.send(res)
        results.append(res)
    return results
        
async def async_run(urls = None, **kwargs):
    urls = urls or URLs
    session = AsyncHTMLSession()
    #browser = pyppeteer.launch({
    #    'ignoreHTTPSErrors':True,
    #    'headless': True,
    #    'handleSIGINT':False,
    #    'handleSIGTERM':False,
    #    'handleSIGHUP':False,
    #    'userDataDir': hp.TEMP_DIR,
    #    'args': ['--no-sandbox']
    #})
    #session._browser = browser
    tasks = (load_url(session, url, description = desc, **kwargs) for url, desc in urls)
    r = await asyncio.gather(*tasks)
    hp.time.sleep(5)
    await session.close()
    return r


def run():
    last_run = hp.to_datetime(hp.get('last_run'))
    rl = []
    logger.info("Before parse table")
    results = asyncio.run(async_run(URLs, last_run = last_run))
    for res in results:
        rl.extend(res)
    hp.save('last_run', str(hp.now()))
    return json.dumps(rl, indent = 4)

if __name__ == "__main__":
    run()

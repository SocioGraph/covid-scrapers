import sys
from os.path import exists as ispath, dirname, basename, join as joinpath, abspath, sep as dirsep, isfile, splitext

sys.path.insert(0, joinpath(dirname(dirname(abspath(__file__)))))
import helpers as hp
import datefinder

logger = hp.get_logger(__name__)
import phonenumbers
from time import sleep
import pandas as pd
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import json

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-gpu")
driver = webdriver.Chrome(chrome_options = chrome_options)
# driver = webdriver.Firefox()
#driver.maximize_window()
driver.set_window_size(1440, 900)

df_covid_data = pd.DataFrame(columns=['category', 'city', 'contact_name', 'phoneNumber', 'description'])


def check_element(element_class, element_name):
    try:
        ret_val = element_class.find_element_by_class_name(element_name).text
    except:
        ret_val = 'NA'

    if ret_val == "" or ret_val == " ":
        ret_val = 'NA'
    return ret_val


def convert_time(string):
    try:
        a = datefinder.find_dates(string.lower())
        c = 0
        for b in a:
            c = b
    except:
        c = 1
    return c


def convert_phone(string):
    lo = []
    str1 = string.replace("/", ",")
    str2 = str1.replace("-", "")
    str3 = str2.replace(" ", "")
    li = list(str3.split(','))
    for l in li:
        try:
            ph_no = phonenumbers.parse(l, "IN").national_number
            lo.append(str(ph_no))
        except:
            pass
    return lo


def scrape(last_data=None):
    driver.get("https://verifiedcovidleads.com/")
    sleep(2)
    tabBar_buttons = driver.find_element_by_id('tabBar').find_elements_by_tag_name('button')
    for i in tabBar_buttons:
        category_val = i.text
        print(category_val)
        i.click()
        cat_count_xpath="//div[@class ='bottom-area']"
        category_counter = driver.find_elements_by_xpath(cat_count_xpath)
        try:
            driver.find_element_by_xpath(cat_count_xpath).text
        except:
            category_counter = None

        while category_counter is not None:
            # Loop to get the struct
            previous_element = check_element(category_counter[0], 'card-title')
            previous_category_counter = category_counter
            for catCount in category_counter:
                desc = check_element(catCount, 'card-subtitle')
                if desc == 'NA':
                    desc = category_val + " available"

                df_covid_data.loc[len(df_covid_data)] = [category_val,
                                                         check_element(catCount, 'bottom-right-overlay'),
                                                         check_element(catCount, 'card-header'),
                                                         check_element(catCount, 'card-title'),
                                                         desc
                                                         ]
            driver.execute_script("arguments[0].scrollIntoView();", category_counter[len(category_counter) - 1])

            category_counter = driver.find_elements_by_xpath(cat_count_xpath)
            # Check if more div elements are present - to be scrolled through
            if previous_element == check_element(category_counter[0],
                                                 'card-title') and previous_category_counter == category_counter:
                category_counter = None
            else:
                category_counter = driver.find_elements_by_xpath(cat_count_xpath)

    driver.quit()

    df_covid_data.drop_duplicates(
        subset=['category', 'city', 'contact_name', 'phoneNumber', 'description'],
        inplace=True)
    df_covid_data.sort_values(["category", "city"], axis=0,
                              ascending=[True, False], inplace=True)
    index_names = df_covid_data[(df_covid_data['phoneNumber'] == 'NA')].index
    df_covid_data.drop(index_names, inplace=True)

    # merging with city file to add state
    data = requests.get('https://raw.githubusercontent.com/kspkalyani/cities_state_india/main/cities.json').json()

    df_city_state = pd.DataFrame.from_dict(data)

    df_covid_data.insert(5, 'state', 'India')
    for ind_1 in df_covid_data.index:
        for ind_2 in df_city_state.index:
            if df_covid_data['city'][ind_1].upper().find(df_city_state['City'][ind_2].upper()) != -1:
                df_covid_data['state'][ind_1] = df_city_state['State'][ind_2]
                break
            elif df_covid_data['city'][ind_1].upper().find(df_city_state['State'][ind_2].upper()) != -1:
                df_covid_data['state'][ind_1] = df_city_state['State'][ind_2]
                break

    index_names = df_covid_data[
        (df_covid_data['state'] == 'India') & (df_covid_data['category'] != 'Doctors on Call')].index
    df_covid_data.drop(index_names, inplace=True)

    df_covid_data_1 = df_covid_data
    df_covid_data_2 = df_covid_data
    if last_data:
        df_last_run_data = pd.read_json(last_data)
        # append last run data to the scrapped data
        df_covid_data_1 = df_covid_data.append(df_last_run_data)
        # remove last run data from scrapped data to send it to the api request
        df_covid_data_1.drop_duplicates(keep=False, inplace=True)
        # add filtered data to last run data to save it in the json file
        df_covid_data_2 = df_last_run_data.append(df_covid_data_1)
        # df_covid_data_2.drop_duplicates(keep='first', inplace=True)

        print(f'df_last_run_data count:{len(df_last_run_data.index)}')

        # df_covid_data.to_json('scraped_data.json')
        # df_last_run_data.to_json('last_run_data.json')
        # df_covid_data_1.to_json('sent_data.json')
        # df_covid_data_2.to_json('return_data.json')
    print(f'scraped data count:{len(df_covid_data.index)}')
    print(f'scraped - last data count:{len(df_covid_data_1.index)}')
    print(f'last data + sent data to filename.json count:{len(df_covid_data_2.index)}')
    error_count = 0
    sent_count = 0
    for index, row in df_covid_data_1.iterrows():
        try:
            hp.send({
                "description": row['description'],
                "category": row['category'],
                "state": row['state'],
                "phoneNumber": convert_phone(row['phoneNumber']),
                "city": row['city']
            })
            sent_count = sent_count + 1
        except:
            error_count = error_count + 1

    print(f'Count of Records loaded is {sent_count} and not loaded: {error_count}')
    return_result = df_covid_data_2.to_json(orient="records")
    return return_result


def run():
    hp.ENVIRONMENT = "dev"
    last_data = hp.get('prev_data')
    results = scrape(last_data)
    hp.save('prev_data', results)
    return results


if __name__ == "__main__":
    run()
